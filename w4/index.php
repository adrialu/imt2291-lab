<?php
session_start();

require_once('classes/User.php');
require_once('classes/DB.php');
require_once('vendor/autoload.php');

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader);

$dbh = DB::getConnection(); // TODO: show an error if we can't connect
$user = new User($dbh);

echo $twig->render('index.html', [
	'loggedin' => $user->loggedIn(),
	'user' => $user,
]);

?>
