<?php

class User {
	public $uid = -1;
	public $email, $password, $givenName, $familyName, $alias;

	/**
	 * Creates a new user object.
	 *
	 * @param dbh a PDO database connection.
	 */
	public function __construct(PDO $dbh){
		$this->dbh = $dbh;

		if(isset($_POST['login'])){
			$this->login($_POST['email'], $_POST['password']);
		} else if(isset($_POST['register'])){
			$this->add([
				'email' => $_POST['email'],
				'alias' => $_POST['alias'],
				'password' => $_POST['password'],
				'givenName' => $_POST['givenName'],
				'familyName' => $_POST['familyName'],
			]);
		} else if(isset($_POST['logout'])){
			unset($_SESSION['uid']);
		} else if(isset($_SESSION['uid'])){
			$this->uid = $_SESSION['uid'];
		}
	}

	public function __destruct(){
		if($this->dbh != null){
			unset($this->dbh);
		}
	}

	/**
	 * @return boolean if the user is logged in or not.
	 */
	public function loggedIn(){
		return $this->uid > -1;
	}

	/**
	 * Creates a new user, given the parameters.
	 *
	 * @param user an array with 'email', 'alias', 'password', 'givenName' and 'familyName'
	 * @return an array with the only element 'status' == 'OK' on success.
	 *         'status' == 'FAIL' on error, the error info can be found in 'errorMessage'.
	 */
	public function add(array $user){
		$res = [];
		$res['status'] = 'FAIL'; // default status

		if(empty($user)){
			$res['errorMessage'] = 'No user data';
		} else if(empty($user['email'])){
			$res['errorMessage'] = 'No email';
		} else if(empty($user['alias'])){
			$res['errorMessage'] = 'No alias';
		} else if(empty($user['password'])){
			$res['errorMessage'] = 'No password';
		} else if(empty($user['givenName'])){
			$res['errorMessage'] = 'No givenName';
		} else if(empty($user['familyName'])){
			$res['errorMessage'] = 'No familyName';
		} else {
			$stmt = 'INSERT INTO w4 (email, password, alias, givenName, familyName) VALUES (?, ?, ?, ?, ?)';
			$query = $this->dbh->prepare($stmt);
			$query->bindValue(1, $user['email'], PDO::PARAM_STR);
			$query->bindValue(2, password_hash($user['password'], PASSWORD_BCRYPT), PDO::PARAM_STR);
			$query->bindValue(3, $user['alias'], PDO::PARAM_STR);
			$query->bindValue(4, $user['givenName'], PDO::PARAM_STR);
			$query->bindValue(5, $user['familyName'], PDO::PARAM_STR);
			$query->execute();

			if($query->rowCount() == 1){
				$res['status'] = 'OK';
				$res['id'] = $this->dbh->lastInsertId();

				// might as well just log the user in directly
				$this->login($user['email'], $user['password']);
			} else {
				$res['errorMessage'] = 'Failed to add user';
			}
		}

		return $res;
	}

	/**
	 * Deletes a user by ID.
	 *
	 * @param userID the ID of the user to delete.
	 * @return an array with the only element 'status' == 'OK' on success.
	 *         'status' == 'FAIL' on error, the error info can be found in 'errorMessage'.
	 */
	public function delete(int $userID){
		$res = [];
		$res['status'] = 'FAIL'; // default status

		if(empty($userID)){
			$res['errorMessage'] = 'No user ID';
		} else {
			$stmt = 'DELETE FROM w4 WHERE id=?';
			$query = $this->dbh->prepare($stmt);
			$query->bindValue(1, $userID, PDO::PARAM_INT);
			$query->execute();

			if($query->rowCount() == 1){
				$res['status'] = 'OK';
			} else {
				$res['errorMessage'] = 'Failed to delete user';
			}
		}

		return $res;
	}

	/**
	 * Verifies if the credentials match a known user.
	 *
	 * @param user username (email) of an existing user.
	 * @param pass password (plain text) of an existing user.
	 * @return an array with the only element 'status' == 'OK' on success.
	 *         'status' == 'FAIL' on error, the error info can be found in 'errorMessage'.
	 */
	public function login(string $user, string $pass){
		$res = [];
		$res['status'] = 'FAIL'; // default status

		if(empty($user)){
			$res['errorMessage'] = 'Empty username';
		} else if(empty($pass)){
			$res['errorMessage'] = 'Empty password';
		} else {
			$stmt = 'SELECT * FROM w4 WHERE email=?';
			$query = $this->dbh->prepare($stmt);
			$query->bindValue(1, $user, PDO::PARAM_STR);
			$query->execute();

			$data = $query->fetch(PDO::FETCH_ASSOC);
			if(!$data){
				$res['errorMessage'] = 'User doesn\'t exist';
			} else if(password_verify($pass, $data['password'])){
				$res['errorMessage'] = 'Password was incorrect';
			} else {
				$res['status'] = 'OK';

				// add the data to the current object
				$this->email = $data['email'];
				$this->password = $data['password'];
				$this->givenName = $data['givenName'];
				$this->familyName = $data['familyName'];
				$this->alias = $data['alias'];
				$this->uid = $data['id'];

				// set the session data so it's counted as a proper login
				$_SESSION['uid'] = $data['id'];
			}
		}

		return $res;
	}
}
