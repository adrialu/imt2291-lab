<?php

require_once('./classes/DB.php');

$dbh = DB::getConnection();
$dbh->exec("
	CREATE TABLE w4(
		id INT(11) AUTO_INCREMENT PRIMARY KEY,
		email VARCHAR(50) NOT NULL,
		password VARCHAR(50) NOT NULL,
		alias VARCHAR(50) NOT NULL,
		givenName VARCHAR(50) NOT NULL,
		familyName VARCHAR(50) NOT NULL
	);
");
