<?php

require_once('./classes/DB.php');
require_once('./classes/User.php');

class UserCest {
	private $testUser = [
		'email' => 'test@test.test',
		'alias' => 'test',
		'password' => 'test',
		'givenName' => 'John',
		'familyName' => 'Doe'
	];

	public function _after(AcceptanceTester $I){
		$dbh = DB::getConnection();
		$user = new User($dbh);
		// $user->delete($_SESSION['uid']);
	}

	// tests
	public function tryToRegister(AcceptanceTester $I){
		$I->amOnPage('/');
		$I->see('Registrer ny bruker');
		$I->fillField('#registerEmail', $this->testUser['email']);
		$I->fillField('#registerPassword', $this->testUser['password']);
		$I->fillField('#registerAlias', $this->testUser['alias']);
		$I->fillField('#registerGivenName', $this->testUser['givenName']);
		$I->fillField('#registerFamilyName', $this->testUser['familyName']);
		$I->click('registrer');
		$I->see('Du er pålogget');
	}

	public function tryToLogIn(AcceptanceTester $I){
		$I->amOnPage('/');
		$I->see('Vennligst logg inn');
		$I->fillField('#loginEmail', $this->testUser['email']);
		$I->fillField('#loginPassword', $this->testUser['password']);
		$I->click('logg inn');
		$I->see('Du er pålogget');
	}

	public function tryToLogOut(AcceptanceTester $I){
		$this->tryToLogIn($I);

		$I->amOnPage('/');
		$I->see('Du er pålogget');
		$I->click('logg ut');
		$I->see('Vennligst logg inn');
	}
}
