<?php

require_once('./classes/DB.php');

class DBTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	// tests
	public function testCanConnectToDB(){
		$this->assertInstanceOf(PDO::class, DB::getConnection());
	}
}
