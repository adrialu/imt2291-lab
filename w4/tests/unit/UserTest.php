<?php

require_once('./classes/DB.php');
require_once('./classes/User.php');

class UserTest extends \Codeception\Test\Unit {
	/**
	 * @var \UnitTester
	 */
	protected $tester;

	protected function _before() {
		$dbh = DB::getConnection();
		$this->user = new User($dbh);
	}

	// tests
	public function testCanAddUser(){
		// add a user to test the functionality
		$res = $this->user->add([
			'email' => 'test@test.test',
			'alias' => 'test',
			'password' => 'test',
			'givenName' => 'John',
			'familyName' => 'Doe'
		]);

		// verify the existence of the user
		$this->assertEquals('OK', $res['status'], 'Failed to create user');
		$this->assertTrue($res['id'] > 0, 'UserID should be > 0');

		// delete the user after the test finished
		$res = $this->user->delete($res['id']);
		$this->assertEquals('OK', $res['status'], 'Failed to delete user');
	}

	public function testCanUserLogin(){
		// add a user to test the functionality
		$res = $this->user->add([
			'email' => 'test@test.test',
			'alias' => 'test',
			'password' => 'test',
			'givenName' => 'John',
			'familyName' => 'Doe'
		]);

		// test a successful login
		$res = $this->user->login('test@test.test', 'test');
		$this->assertEquals('OK', $res['status'], 'Failed to login with user');

		// test when we use an invalid user name
		$res = $this->user->login('test2@test.test', '');
		$this->assertEquals('FAIL', $res['status'], 'Should have failed, the user doesn\'t exist');

		// test when we have an incorrect password
		$res = $this->user->login('test1@test.test', 'invalid');
		$this->assertEquals('FAIL', $res['status'], 'Should have failed, the password is incorrect');

		// test when we have an empty password but correct user
		$res = $this->user->login('test@test.test', '');
		$this->assertEquals('FAIL', $res['status'], 'Should have failed, the password is empty');

		// test when we have an empty user name
		$res = $this->user->login('', '');
		$this->assertEquals('FAIL', $res['status'], 'Should have failed, the username is empty');

		// delete the user after the test finished
		$res = $this->user->delete($this->user->uid);
		$this->assertEquals('OK', $res['status'], 'Failed to delete user');
	}
}
