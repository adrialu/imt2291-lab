<?php

class DB {
	private static $db = null;
	private $dsn = 'mysql:dbname=imt2291;host=127.0.0.1;port=3306';
	private $user = 'imt2291';
	private $pass = 'imt2291';
	private $dbh = null;

	private function __construct(){
		try {
			$this->dbh = new PDO($this->dsn, $this->user, $this->pass);
			$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch(PDOException $e){
			// NOTE: IKKE BRUK DETTE I PRODUKSJON
			die('Connection failed: ' . $e->getMessage());
		}
	}

	/**
	 * Returns a database connection, creating it if it doesn't exist.
	 */
	public static function getConnection(){
		if(DB::$db == null){
			DB::$db = new self();
		}
		return DB::$db->dbh;
	}
}
