<?php

require_once('vendor/autoload.php');

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader);

libxml_use_internal_errors(true); // suppress warnings

$data = [];
$dom = new DOMDocument();

if(isset($_GET['feedURL']) and !empty($_GET['feedURL'])){
	$dom->loadHTMLFile(format_url($_GET['feedURL']));
	$x = new DOMXpath($dom);

	$feeds = [];
	foreach($x->query('//link[@rel="alternate" and @type="application/rss+xml"]') as $feed){
		$feeds[] = [
			'title' => $feed->getAttribute('title'),
			'href' => $feed->getAttribute('href'),
		];
	}

	$data['url'] = $_GET['feedURL'];
	$data['feeds'] = $feeds;
} elseif(isset($_GET['readURL']) and !empty($_GET['readURL'])){
	$dom->loadHTMLFile($_GET['readURL']);
	$x = new DOMXpath($dom);

	$articles = [];
	foreach($x->query('//item') as $article){
		$articles[] = [
			'title' => $x->query('./title', $article)->item(0)->textContent,
			'image' => $x->query('./image', $article)->item(0)->textContent,
			'content' => $article->textContent,
		];
	}

	$data['url'] = $_GET['readURL'];
	$data['articles'] = $articles;
}

echo $twig->render('oppgave1.html', $data);

function format_url($url){
	$url = parse_url($url);
	// https://secure.php.net/manual/en/function.parse-url.php
	// with some modifications
	$scheme   = isset($url['scheme'])   ?       $url['scheme'] . '://' : 'http://';
	$host     = isset($url['host'])     ?       $url['host']           : '';
	$port     = isset($url['port'])     ? ':' . $url['port']           : '';
	$user     = isset($url['user'])     ?       $url['user']           : '';
	$pass     = isset($url['pass'])     ? ':' . $url['pass']           : '';
	$pass     = ($user || $pass)               ? "$pass@"                            : '';
	$path     = isset($url['path'])     ?       $url['path']           : '';
	$query    = isset($url['query'])    ? '?' . $url['query']          : '';
	$fragment = isset($url['fragment']) ? '#' . $url['fragment']       : '';
	return "$scheme$user$pass$host$port$path$query$fragment";
} 
