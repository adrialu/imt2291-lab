<?php

require_once('./classes/DB.php');

$dbh = DB::getConnection();
$dbh->exec("
	CREATE TABLE w5(
		id INT(11) AUTO_INCREMENT PRIMARY KEY,
		thumbnail BLOB,
		name VARCHAR(200) NOT NULL,
		size INT(11) NOT NULL,
		mime VARCHAR(100) NOT NULL
	);
");
