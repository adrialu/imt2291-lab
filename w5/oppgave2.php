<?php

require_once('vendor/autoload.php');
require_once('classes/DB.php');

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader);

$dbh = DB::getConnection();

// Lag en side hvor brukeren kan laste opp bilder.
// Bildene skal lagres på disk mens en thumbnailutgave av bildet skal lagres i databasen.
// Thumbnailbildet skaleres til 75x75 punkter.
// Vis en oversikt over alle bilder med thumbnailbildene og tittel på bildene.
// La brukeren laste ned det originale bildet.

@mkdir('bilder/'); // needs access to the parent directory

if(isset($_FILES['bilde'])){
	if(!is_uploaded_file($_FILES['bilde']['tmp_name'])){
		echo 'What are you doing?<br>';
		return;
	}

	try {
		$thumbnail = create_thumbnail(file_get_contents($_FILES['bilde']['tmp_name']));

		// upload a thumbnail to the database
		$query = $dbh->prepare('INSERT INTO w5(thumbnail, name, size, mime) VALUES(?, ?, ?, ?)');
		$query->bindValue(1, $thumbnail, PDO::PARAM_LOB);
		$query->bindValue(2, $_FILES['bilde']['name'], PDO::PARAM_STR);
		$query->bindValue(3, strlen($thumbnail), PDO::PARAM_INT);
		$query->bindValue(4, $_FILES['bilde']['type'], PDO::PARAM_STR);
		$query->execute();
		$id = $dbh->lastInsertId();

		echo "database id for row with thumb: $id<br>";

		// copy the original image to disk, named by the thumbnail's ID in the database
		move_uploaded_file($_FILES['bilde']['tmp_name'], 'bilder/' . $id);

		echo 'OK';
	} catch(PDOException $e){
		echo 'Cannot upload image<br>';
	}
} elseif(isset($_GET['download'])){
	$query = $dbh->prepare('SELECT * FROM w5 WHERE id=?');
	$query->bindValue(1, $_GET['download'], PDO::PARAM_INT);

	if($query->execute() and file_exists('bilder/' . $_GET['download'])){
		$row = $query->fetch(PDO::FETCH_ASSOC);

		header('Content-Type: ' . $row['mime']);
		header('Content-Disposition: attachment; filename=' . $row['name']);
		header('Content-Length: ' . filesize('bilder/' . $_GET['download']));
		readfile('bilder/' . $_GET['download']);
		die();
	}
} elseif(isset($_GET['thumb'])){
	$query = $dbh->prepare('SELECT * FROM w5 WHERE id=?');
	$query->bindValue(1, $_GET['thumb'], PDO::PARAM_INT);

	if($query->execute()){
		$row = $query->fetch(PDO::FETCH_ASSOC);

		header('Content-Type: ' . $row['mime']);
		header('Content-Disposition: attachment; filename=' . $row['name']);
		header('Content-Length: ' . $row['size']);
		echo $row['thumbnail'];
		die();
	}
}

$images = [];
$query = $dbh->query('SELECT * FROM w5');
if($query and $query->rowCount()){ // check if the query was successful AND there are entries
	foreach($query->fetchAll(PDO::FETCH_ASSOC) as $image){
		$images[] = [
			'id' => $image['id'],
		];
	}
}

echo $twig->render('oppgave2.html', [
	'images' => $images,
]);

function create_thumbnail($src){
	// get source image as blob
	$src = imagecreatefromstring($src);
	$src_width = imagesx($src);
	$src_height = imagesy($src);

	// target size
	$width = 75;
	$height = floor($src_height * ($width / $src_width));

	// buffer image
	$thumbnail = imagecreatetruecolor($width, $height);

	// resample into thumbnail
	imagecopyresampled($thumbnail, $src, 0, 0, 0, 0, $width, $height, $src_width, $src_height);

	// create buffer image
	ob_start();
	imagepng($thumbnail, null, 9);
	$thumbnail = ob_get_contents();
	ob_end_clean();

	return $thumbnail;
}
