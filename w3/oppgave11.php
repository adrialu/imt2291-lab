<?php

// legg til på siden som viser alle kontakter, en link som
// tar deg til en side for å redigere kontakter.
// la brukeren redigere og oppdatere en kontakt.

require_once('db_props.php');
require_once('vendor/autoload.php');

$db = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST . ';charset=utf8',
	DB_USER, DB_PASS, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

if(isset($_POST['edit'])){
	$data = $_POST;

	$query = $db->prepare("UPDATE w3 SET name=?, email=?, tel=? WHERE id=?");
	$query->bindValue(1, $data['name'], PDO::PARAM_STR);
	$query->bindValue(2, $data['email'], PDO::PARAM_STR);
	$query->bindValue(3, $data['tel'], PDO::PARAM_INT);
	$query->bindValue(4, $data['id'], PDO::PARAM_INT);

	$query->execute();
	// don't stop here, return the list again
} elseif(isset($_GET['edit'])){
	$query = $db->prepare('SELECT * FROM w3 WHERE id=?');
	$query->bindValue(1, $_GET['edit'], PDO::PARAM_INT);
	if($query->execute()){
		$user = $query->fetch(PDO::FETCH_ASSOC);
		if($user){
			echo $twig->render('oppgave11.html', ['user' => $user]);
			return;
		}
	}
}

$query = $db->prepare('SELECT * FROM w3');
if($query->execute()){
	$users =$query->fetchAll(PDO::FETCH_ASSOC);
	echo $twig->render('oppgave11.html', ['users' => $users]);
}
