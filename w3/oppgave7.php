<?php

// Bruk fortsatt twig, lag en side med en form for å legge til kontakter

require_once('vendor/autoload.php');

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

echo $twig->render('oppgave7.html', array());
