<?php

// med twig, lag en side som viser alle kontakter i database

require_once('db_props.php');
require_once('vendor/autoload.php');

$db = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST . ';charset=utf8',
	DB_USER, DB_PASS, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

$query = $db->prepare('SELECT * FROM w3');
if($query->execute()){
	$users =$query->fetchAll(PDO::FETCH_ASSOC);
	echo $twig->render('oppgave9.html', ['users' => $users]);
}
