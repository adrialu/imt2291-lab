<?php

// med twig, lag en side som lar deg søke etter kontakter
// og vise kontakter som matcher søket (alt på en og
// samme side)

require_once('db_props.php');
require_once('vendor/autoload.php');

$db = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST . ';charset=utf8',
	DB_USER, DB_PASS, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

$users = array();

if(isset($_GET['search'])){
	$query = $db->prepare("SELECT * FROM w3 WHERE name LIKE ?");
	$query->bindValue(1, '%'.$_GET['search'].'%', PDO::PARAM_STR);
	$query->execute();

	$users = $query->fetchAll(PDO::FETCH_ASSOC);
}

echo $twig->render('oppgave10.html', ['users' => $users]);
