<?php

// Lag en side som tar i mot data om en kontakt og legger dette i databasen

require_once('db_props.php');

$db = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST . ';charset=utf8',
	DB_USER, DB_PASS, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

if(isset($_POST['new'])){
	$query = $db->prepare('INSERT INTO w3(name, email, tel) VALUES(?, ?, ?)');
	$query->bindValue(1, $_POST['name'], PDO::PARAM_STR);
	$query->bindValue(2, $_POST['email'], PDO::PARAM_STR);
	$query->bindValue(3, $_POST['tel'], PDO::PARAM_INT);
	if($query->execute()){
		echo "Contact added!";
	} else {
		echo "Something went wrong!";
	}
}
