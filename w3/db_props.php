<?php

// phpinfo();

const DB_HOST = 'localhost';
const DB_USER = 'imt2291';
const DB_PASS = 'imt2291';
const DB_ROOT_USER = 'root';
const DB_ROOT_PASS = 'imt2291';
const DB_NAME = 'imt2291';

// create a database and user if they don't already exist
try {
	$cmd = sprintf("
		CREATE DATABASE IF NOT EXISTS %s;
		GRANT USAGE ON *.* TO %s@'%%' IDENTIFIED BY '%s';
		GRANT ALL PRIVILEGES ON %s.* TO %s@'%%';
		FLUSH PRIVILEGES;
	", DB_NAME, DB_USER, DB_PASS, DB_NAME, DB_USER);

	$db = new PDO("mysql:host=" . DB_HOST . ";charset=utf8", DB_ROOT_USER, DB_ROOT_PASS);
	$db->exec($cmd);
} catch (PDOException $e){
	die("DB ERROR: " . $e->getMessage());
}
