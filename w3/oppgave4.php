<?php

require_once 'vendor/autoload.php';

use seregazhuk\PinterestBot\Factories\PinterestBot;

class Pinterest {
	public static function getPins($searchString){
		$bot = PinterestBot::create();

		$pins = $bot->pins
			->search(str_replace(" ", "%20", $searchString))
			->take(25)
			->toArray();

		$tmp = [];
		foreach($pins as $pin) {
			$tmp[] = array($pin['images']['orig']['url'], $pin['rich_summary']['url']);
		}
		return $tmp;
	}
}

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

echo $twig->render('oppgave4.html', ['pins' => Pinterest::getPins("mathematical riddles fun")]);
