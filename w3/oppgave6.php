<?php

// Lag en database med en tabell for kontakter (navn, epost, tlf....)

require_once('db_props.php');

$db = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST . ';charset=utf8',
	DB_USER, DB_PASS, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

$db->exec("
	CREATE TABLE w3(
		id INT(11) AUTO_INCREMENT PRIMARY KEY,
		name VARCHAR(50) NOT NULL,
		email VARCHAR(50) NOT NULL,
		tel INT NOT NULL
	);
");
