### Live Reload

Make sure to include `Live.js` script in the markup:

	<script type="text/javascript" src="http://livejs.com/live.js"></script>

Run a web server to access the markup:

	python2 -m SimpleHTTPServer 8000

Navigate to <localhost:8000> and find the markup file.

### Run Apache + PHP in current directory with Docker

Navigate to the directory and run this:

	docker run -itd --name imt2291 --restart unless-stopped -v "$PWD":/var/www/html -p 8000:80 php:apache

Navigate to <localhost:8000>

Kill it with:

	docker rm -f imt2291

### Run MariadDB with Docker and link with Apache

Browse to the directory you want the WWW root to be, and run:

	docker run -itd --name imt2291 --restart unless-stopped -v "$PWD":/var/www/localhost/htdocs -p 8000:80 -p 3306:3306 -e TIMEZONE=Europe/Oslo -e MYSQL_ROOT_PASSWORD=imt2291 -v "$PWD/database":/var/lib/mysql -v /dev/null:/var/lib/mysql/test glats/alpine-lamp

Visit <localhost:8000>

Now you can access the database in PHP under the hostname `db`, using the username and password `imt2291`.  
Such secure, much wow.

Even though this is persistent, the users are for some reason not being stored persistently.

### Twig

	composer require "twig/twig:^2.0"

### Codeception

Install it with composer (in the subdirectory for the project, e.g. `w4`):

	composer require --dev codeception/codeception

Bootstrap it

	/vendor/bin/codecept bootstrap

Remove `/tests/functional.suite.yml`, and add the root-URL in `/tests/acceptance.suite.yml` (as the bootstrap said).

Create a Unit test

	/vendor/bin/codecept g:cest unit DB

Modify the test, then run it

	/vendor/bin/codecept run

Create acceptance tests (I see, I click, etc), virtual browser.
